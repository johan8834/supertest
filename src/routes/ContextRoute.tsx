import { useCounter } from "../hooks/use-context";

const ContextRoute = () => {
  const counter = useCounter();

  return (
    <div>
      <h1>{counter.count}</h1>

      <button onClick={() => counter.handleClick()}>Click</button>
    </div>
  );
};

export default ContextRoute;
