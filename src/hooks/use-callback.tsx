import { useCallback, useEffect, useState } from "react";

export const UseCallbackPage = () => {
  const [number, setNumber] = useState(0);
  const [dark, setDark] = useState(false);

  const getItems = useCallback(() => {
    return [number, number + 1, number + 2];
  }, [number]);

  const theme = dark ? "dark" : "light";

  return (
    <div>
      <input
        value={number}
        onChange={(e) => setNumber(Number(e.target.value))}
      />
      <hr />
      <button onClick={() => setDark(!dark)}>Toggle {theme}</button>
      <hr />
      <Lists getItems={getItems} />

      <hr />
    </div>
  );
};

const Lists = ({ getItems }: { getItems: () => number[] }) => {
  const [items, setItems] = useState(getItems());

  useEffect(() => {
    setItems(getItems());

    // it's rerendering everytime the parent compoent changed
    console.log("fuck you");
  }, [getItems]);

  return (
    <div>
      {items.map((it) => (
        <div key={it}>{it}</div>
      ))}
    </div>
  );
};
