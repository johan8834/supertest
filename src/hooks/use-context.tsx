import { ReactNode, createContext, useContext, useState } from "react";

interface CounterType {
  count: number;
  handleClick: () => void;
}

const counterContext = createContext<CounterType>({
  handleClick: () => {},
  count: 0,
});

export const CounterProvider = ({ children }: { children: ReactNode }) => {
  const [count, setCount] = useState(0);

  const handleClick = () => {
    setCount((prev) => prev + 1);
  };

  return (
    <counterContext.Provider value={{ count, handleClick }}>
      {children}
    </counterContext.Provider>
  );
};

export const useCounter = () => {
  return useContext(counterContext);
};
