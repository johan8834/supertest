import { useState, useTransition } from "react";

export const UseTransitionPage = () => {
  const [isPending, stratTransition] = useTransition();
  const [input, setInput] = useState(0);
  const [list, setList] = useState<number[]>([]);

  return (
    <div>
      <input
        value={input}
        onChange={(e: any) => {
          setInput(e.target.value);
          let l: any[] = [];

          // leave this for a while, unless both will take long but now it shows loading and the input is fast
          stratTransition(() => {
            for (let i = 0; i <= 20000; i++) {
              l.push(e.target.value);
            }
            setList(l);
          });
        }}
      />
      <hr />

      {isPending
        ? "Loading"
        : list.map((li, index) => <div key={index}>{li}</div>)}
    </div>
  );
};
