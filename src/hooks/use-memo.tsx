import { useMemo, useState } from "react";

export const UseMemoPage = () => {
  const [number, setNumber] = useState(0);
  const [dark, setDark] = useState(true);

  // without useMemo, it's recomputing everytime the component rerender (dark mode took so long)
  // const result = superExpensive(number);

  // with use memo it will only rerender only if the number has changed (dark mode won't be affected), cool right?
  const result = useMemo(() => {
    return superExpensive(number);
  }, [number]);

  return (
    <div>
      <h1>Theme :{dark ? "Dark" : "Light"}</h1>
      <h1>Result : {result}</h1>

      <br />

      <input
        placeholder=""
        onChange={(e) => setNumber(Number(e.target.value))}
      />
      <br />
      <button onClick={() => setDark(!dark)}>Toggle Theme</button>
    </div>
  );
};

function superExpensive(input: number) {
  for (let i = 0; i <= 1000000000; i++) {}

  return input * 2;
}
