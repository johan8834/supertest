import { UseTransitionPage } from "./hooks/use-transition";

const App = () => {
  return (
    <div>
      {/* <UseMemoPage /> */}

      {/* <UseCallbackPage /> */}

      <UseTransitionPage />
    </div>
  );
};

export default App;
